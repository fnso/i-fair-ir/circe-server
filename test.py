from circe.config import CONFIG
import circe_client
from circe import make_api_access, remove_api_access
import json
import os
import pytest

credentials = {}
client: circe_client.Client = None


def setup_module():
    global credentials
    global client
    make_api_access(title="test client", out="test_client_credentials.json")
    credentials = json.load(open("test_client_credentials.json", "rb"))
    client = circe_client.Client(
        api_endpoint="http://{}:{}/".format(CONFIG["CIRCE_HOST"], CONFIG["CIRCE_PORT"]),
        secret_key=credentials["secret"],
        application_uuid=credentials["uuid"],
    )


def teardown_module():
    global credentials
    remove_api_access(credentials["uuid"])
    os.remove("test_client_credentials.json")


def test_get_available_transformations():
    global client
    transformations = client.available_transformations()
    assert type(transformations) == dict


def test_blocking_call_to_post_job():
    global client
    job = client.new_job()
    job.add_file("test_assets/index.html")
    job.add_file("test_assets/style.css")
    job.add_transformation("sleep_well", {"time": 1})
    client.send(job, wait=True, destination_file="test_assets/result.tar.gz")
    assert job.result_file_path is not None


def test_non_blocking_call_to_post_job():
    global client
    job = client.new_job()
    job.add_file("test_assets/doc.docx")
    job.add_transformation("sleep_well", {"time": 1})
    client.send(job)
    assert job.uuid is not None


def test_polling():
    global client
    job = client.new_job()
    job.add_file("test_assets/doc.docx")
    job.add_transformation("sleep_well", {"time": 5})
    client.send(job)
    client.poll(job, "test_assets/result.tar.gz")
    assert os.path.isfile("test_assets/result.tar.gz")


def test_log_presence():
    global client
    job = client.new_job()
    job.add_file("test_assets/doc.docx")
    job.add_transformation("sleep_well", {"time": 1})
    client.send(job, wait=True)
    file_names = []
    for file_name, _ in job.result.files:
        file_names.append(file_name)
    assert "out.log" in file_names


def test_bad_auth():
    if CONFIG["CIRCE_USE_AUTH"]:
        bad_client = circe_client.Client(
            api_endpoint="http://{}:{}/".format(
                CONFIG["CIRCE_HOST"], CONFIG["CIRCE_PORT"]
            ),
            secret_key="doesnotexist",
            application_uuid="notevenauuid",
        )
        job = bad_client.new_job()
        job.add_file("test_assets/doc.docx")
        job.add_transformation("sleep_well", {"time": 5})
        with pytest.raises(circe_client.AccessDenied) as e:
            bad_client.send(job)
